<?php

namespace MailTact;

use JsonException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Client
{
    public const OPTION_CUSTOM_FORM_NAME = 'X-Custom-Form-Name';
    public const OPTION_CUSTOM_FORM_SUBJECT = 'X-Custom-Form-Subject';

    /**
     * @var string
     */
    private $host = 'https://mailtact.dev';
    /**
     * @var string
     */
    private $formOrigin= '';
    /**
     * @var int
     */
    private $formId = 0;
    /**
     * @var string
     */
    private $formSecret = '';
    /**
     * @var OptionsResolver
     */
    private $optionsResolver;
    /**
     * @var array
     */
    private $options = [];

    public function __construct(string $formOrigin, int $formId, string $formSecret, ?string $host = null)
    {
        if (null !== $host) $this->host = $host;
        $this->formOrigin = $formOrigin;
        $this->formId = $formId;
        $this->formSecret = $formSecret;

        $validator = function(string $value) {
            $value = trim($value);
            if (empty($value)) return false;
            return true;
        };
        $this->optionsResolver = (new OptionsResolver())
            ->setDefined([
                self::OPTION_CUSTOM_FORM_NAME,
                self::OPTION_CUSTOM_FORM_SUBJECT,
            ])
            ->setAllowedTypes(self::OPTION_CUSTOM_FORM_NAME, 'string')
            ->setAllowedTypes(self::OPTION_CUSTOM_FORM_SUBJECT, 'string')
            ->setAllowedValues(self::OPTION_CUSTOM_FORM_NAME, $validator)
            ->setAllowedValues(self::OPTION_CUSTOM_FORM_SUBJECT, $validator)
        ;
    }

    /**
     * @param string $option
     * @param string $value
     * @return $this
     *
     * @see Client::OPTION_CUSTOM_FORM_NAME
     * @see Client::OPTION_CUSTOM_FORM_SUBJECT
     */
    public function setOption(string $option, string $value): self
    {
        $this->options = $this->optionsResolver->resolve(
            array_merge($this->options, [
                $option => $value
            ])
        );
        return $this;
    }

    /**
     * @param array $data
     * @return Response
     * @throws JsonException
     * @throws MailTactFailedException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function send(array $data): Response
    {
        $headers = array_merge([
            'origin' => $this->formOrigin
        ], $this->options);

        $client = HttpClient::create();
        $url = sprintf('%s/collector/api/%d/%s', $this->host, $this->formId, $this->formSecret);
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'json' => $data
        ]);
        $content = $response->getContent(false);
        $json = json_decode($content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JsonException('Invalid json received.');
        }
        if (!$json['success'] ?? false) throw new MailTactFailedException($json['message']);
        return new Response($json['success'], $json['message']);
    }
}