<?php

namespace MailTact;

use Exception;
use Throwable;

class MailTactFailedException extends Exception
{
    public function __construct($message, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}