<?php

namespace MailTact;

interface MailTactObjectInterface
{
    public function toArray(): array;
}