<?php

namespace MailTact;

use JsonException;
use Symfony\Component\Form\FormInterface;

class SymfonyClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * SymfonyClient constructor.
     *
     * @param string $formOrigin
     * @param int $formId
     * @param string $formSecret
     * @param string|null $host
     */
    public function __construct(string $formOrigin, int $formId, string $formSecret, ?string $host = null)
    {
        $this->client = new Client($formOrigin, $formId, $formSecret, $host);
    }

    /**
     * @param string $option
     * @param string $value
     * @return $this
     *
     * @see Client::OPTION_CUSTOM_FORM_NAME
     * @see Client::OPTION_CUSTOM_FORM_SUBJECT
     */
    public function setOption(string $option, string $value): self
    {
        $this->client->setOption($option, $value);
        return $this;
    }

    /**
     * @param FormInterface $form
     * @param string[] $exclude
     * @return Response
     * @throws JsonException
     */
    public function send(FormInterface $form, array $exclude = ['_token']): Response
    {
        $formData = $form->getData();
        if ($formData instanceof MailTactObjectInterface) {
            $data = $formData->toArray();
        } else {
            $data = [];
            foreach($form->getData() as $field => $value) {
                if (in_array($field, $exclude)) continue;
                $data[$field] = $value;
            }
        }
        return $this->client->send($data);
    }
}