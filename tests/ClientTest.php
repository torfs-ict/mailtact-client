<?php

namespace Tests;

use MailTact\Client;
use MailTact\Response;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;

class ClientTest extends TestCase
{
    private function getData(): array
    {
        return [
            'name' => 'John Smith',
            'email' => 'john@acme.com',
            'message' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum dignissim non enim vel pretium. 
                
                Phasellus posuere enim vel enim pretium, ut lobortis sem imperdiet. Nunc ornare nec elit vel consequat. 
                
                Phasellus placerat semper lectus, nec congue leo ornare sit amet. 
            "
        ];
    }

    public function testSuccessfulRequest()
    {
        $client = new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site');
        $response = $client->send($this->getData());
        $this->assertInstanceOf(Response::class, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertNull($response->getMessage());
    }

    public function testSuccessfulRequestWithCustomFormName()
    {
        $client = (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption(Client::OPTION_CUSTOM_FORM_NAME, 'Custom form')
        ;
        $response = $client->send($this->getData());
        $this->assertInstanceOf(Response::class, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertNull($response->getMessage());
    }

    public function testSuccessfulRequestWithCustomFormSubject()
    {
        $client = (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption(Client::OPTION_CUSTOM_FORM_SUBJECT, 'Custom subject')
        ;
        $response = $client->send($this->getData());
        $this->assertInstanceOf(Response::class, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertNull($response->getMessage());
    }

    public function testSuccessfulRequestWithCustomFormNameAndSubject()
    {
        $client = (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption(Client::OPTION_CUSTOM_FORM_NAME, 'Custom form')
            ->setOption(Client::OPTION_CUSTOM_FORM_SUBJECT, 'Custom subject')
        ;
        $response = $client->send($this->getData());
        $this->assertInstanceOf(Response::class, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertNull($response->getMessage());
    }

    public function testAnExceptionIsThrownWhenPassingAnInvalidOption()
    {
        $this->expectException(UndefinedOptionsException::class);
        (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption('X-Unknown-Option', 'test')
        ;
    }

    public function testAnExceptionIsThrownWhenPassingABlankOption()
    {
        $this->expectException(InvalidOptionsException::class);
        (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption(Client::OPTION_CUSTOM_FORM_NAME, '')
        ;
    }

    public function testAnExceptionIsThrownWhenPassingAWhiteSpaceOnlyOption()
    {
        $this->expectException(InvalidOptionsException::class);
        (new Client('https://rijschooldereys.ddev.site', 1, '123456', 'https://mailtact.ddev.site'))
            ->setOption(Client::OPTION_CUSTOM_FORM_NAME, '     ')
        ;
    }
}